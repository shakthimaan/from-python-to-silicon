SOURCE = from-python-to-silicon

all:
	pdflatex $(SOURCE).tex

clean:
	rm -f *.aux *.log *.dvi *.nav *.out *.pdf *.snm *.toc *.vrb *~
	rm -f slides/*~